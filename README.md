# ESIT Training 
Lab 02: commands inside the container

---


## Instructions

 - Download Ubuntu in a Docker container (detached mode) using:
```
$ docker run -d --name myOS ubuntu:latest

*Notice that the container is turnedOff (because no app is running on this container)
```

 - Ensure the container exists:
```
$ docker ps -a
```

 - Run the application in a Docker container (detached mode) using:
```
$ docker run --name myRunningOS -d ubuntu /bin/sh -c "while true; do ping 8.8.8.8; done"

```

 - Attach to the container process using:
```
$ docker attach --detach-keys="ctrl-c"  myRunningOS
```

 - Exit from the container by using :
```
$ (CTRL + C)
```

 - Execute the terminal (interactive) inside the container using:
```
$ docker exec -it myRunningOS /bin/bash
```

 - Inspect the container filesystem:
```
$ ls -l
$ pwd
$ cd /
$ ls -l
```

 - Exit from the terminal using:
```
$ exit
```
